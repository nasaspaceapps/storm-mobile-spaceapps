package com.spaceapps_csb.storm;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.content.Intent;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.kml.KmlLayer;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private TrackGPS gps;
    double longitude;
    double latitude;
    private GoogleMap mMap;
    Marker marker;
    MarkerOptions mo;
    Button btn,btn2,btn3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        //btn
        //btn = (Button)findViewById(R.id.btnAct1);
        btn2= (Button)findViewById(R.id.btnAct2);
        btn3=(Button)findViewById(R.id.btnAct3);

        //btn.setOnClickListener(new View.OnClickListener(){

            //@Override
            //public void onClick(View v) {
                //Intent intent  = new Intent(MapsActivity.this,MapsActivity.class);
               //startActivity(intent);
            //}
        //});
        btn2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent launchIntent = getPackageManager().getLaunchIntentForPackage("com.a5corp.weather");
                if (launchIntent != null) {
                    startActivity(launchIntent);
                }
            }
        });
        btn3.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MapsActivity.this,Safety.class);
                startActivity(intent);
            }
        });
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        getLocation();
    }


    private void getLocation(){
        gps = new TrackGPS(MapsActivity.this);


        if(gps.canGetLocation()){


            longitude = gps.getLongitude();
            latitude = gps .getLatitude();

        }
        else
        {

            gps.showSettingsAlert();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mo = new MarkerOptions().position(new LatLng(14.563806, 120.994843)).title("My Current Location");
        marker = mMap.addMarker(mo);

        LatLng latLng = new LatLng(14.563806, 120.994843);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10.2f));


    }
    public void getEvacuation(View v) {
        mo = new MarkerOptions().position(new LatLng(14.5997941, 120.9920683)).title("St.Maur's Manila");
        marker = mMap.addMarker(mo);

        mo = new MarkerOptions().position(new LatLng(114.6524343, 121.1169774)).title("St. Scholastica's Academy in Marikina");
        marker = mMap.addMarker(mo);

        mo = new MarkerOptions().position(new LatLng(14.6575712, 121.1038373)).title("H. Bautista Elementary School");
        marker = mMap.addMarker(mo);

        mo = new MarkerOptions().position(new LatLng(14.7541, 121.083)).title("Parang Elementary School In Tumana");
        marker = mMap.addMarker(mo);

        mo = new MarkerOptions().position(new LatLng(14.657404, 121.111629)).title(" Parang Elementary School ");
        marker = mMap.addMarker(mo);

        mo = new MarkerOptions().position(new LatLng(14.6502445, 121.0943664)).title("Malanday Elementary School in Malanday.");
        marker = mMap.addMarker(mo);

        mo = new MarkerOptions().position(new LatLng(14.6142525, 121.030582)).title(" Mt. Carmel Parish in New Manila");
        marker = mMap.addMarker(mo);

        mo = new MarkerOptions().position(new LatLng(14.7064754, 121.0957476)).title("The Mother of Divine Providence Parish in Payatas");
        marker = mMap.addMarker(mo);

        mo = new MarkerOptions().position(new LatLng(14.6262558, 121.0101892)).title("Sto. Domingo Church in Quezon City");
        marker = mMap.addMarker(mo);

        mo = new MarkerOptions().position(new LatLng(14.64449, 121.0774648)).title("Miriam College High School");
        marker = mMap.addMarker(mo);

        mo = new MarkerOptions().position(new LatLng(14.631642, 121.073106)).title("Bridget School in Quezon City");
        marker = mMap.addMarker(mo);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        gps.stopUsingGPS();
    }
}
